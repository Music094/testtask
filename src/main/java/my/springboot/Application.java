package my.springboot;

import my.springboot.domain.Parameters;
import my.springboot.service.*;
import my.springboot.service.Interfaces.ConvertFromFile;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.List;


@SpringBootApplication
public class Application implements CommandLineRunner {

    //This list available on the conditional
    //@Autowired
    //private List<String> dataList;

    @Autowired
    private Parameters parameters;

    @Autowired
    private ConvertFromFile convert;

    @Autowired
    ResultWriter resultWriter;


    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build()));
    }

    public static void main(String []args) throws Exception {

        SpringApplication.run(Application.class,args);

    }


    @Override
    public void run(String... args) throws Exception {
        List<String> dataList = new ArrayList<>();
        dataList.add("0900 1730");
        dataList.add(" ");
        dataList.add("2011-03-17 10:17:06 EMP001");
        dataList.add(" ");
        dataList.add("2011-03-21 09:00 2");
        dataList.add(" ");
        dataList.add("2011-03-16 12:34:56 EMP002");
        dataList.add(" ");
        dataList.add("2011-03-21 09:00 2");
        dataList.add(" ");
        dataList.add("2011-03-16 09:28:23 EMP003");
        dataList.add(" ");
        dataList.add("2011-03-22 14:00 2");
        dataList.add(" ");
        dataList.add("2011-03-17 11:23:45 EMP004");
        dataList.add(" ");
        dataList.add("2011-03-22 16:00 1");
        dataList.add(" ");
        dataList.add("2011-03-15 17:29:12 EMP005");
        dataList.add(" ");
        dataList.add("2011-03-21 16:00 3");


        parameters = convert.ConverterIntoParameters(dataList);//conversion

        parameters.setEntriesCustomerses(parameters.sortEntriesCustomersByRequestTime());//sort

        parameters.setEntriesCustomerses(parameters.getResultList());//get result

        parameters.setEntriesCustomerses(parameters.sortEntriesCustomersByMettingTime());//sort

        resultWriter.doWrite(parameters);//write result

    }
}
