package my.springboot.domain;


import lombok.Data;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;


@Component
@Data
public class Parameters {

    private String workTime;
    private List<EntriesCustomers> entriesCustomerses;

    public void showMe(){

        System.out.println("work time = " + workTime);
        for (EntriesCustomers ent: entriesCustomerses) {
            System.out.println("время подачи - " + ent.getRequestSubmissionTime().getTime() + " id = " + ent.getEmployeeId() +
                    " время начала = " + ent.getMeetingStartTime().getTime() + " продолжительность = " + ent.getMeetingDuration());
        }
    }

    public ArrayList<EntriesCustomers> sortEntriesCustomersByRequestTime(){

        EntriesCustomers []temp = new EntriesCustomers[this.entriesCustomerses.size()];

        for (int i = 0; i < this.entriesCustomerses.size() ; i++) {

            temp[i] = this.entriesCustomerses.get(i);
        }

        for(int i = 0; i < temp.length; i++){

            for (int j = 0; j < i ; j++) {

                if(temp[i].compareTo(temp[j]) == -1){

                    EntriesCustomers swap = temp[j];
                    temp[j] = temp[i];
                    temp[i] = swap;

                }
            }
        }

        ArrayList<EntriesCustomers> result = new ArrayList<EntriesCustomers>();

        for(int i = 0;i<temp.length;i++){

            result.add(temp[i]);
        }

        return result;

    }

    public ArrayList<EntriesCustomers> sortEntriesCustomersByMettingTime(){

        EntriesCustomers []temp = new EntriesCustomers[this.entriesCustomerses.size()];

        for (int i = 0; i < this.entriesCustomerses.size() ; i++) {

            temp[i] = this.entriesCustomerses.get(i);

        }

        for(int i = 0; i < temp.length; i++){

            for (int j = 0; j < i ; j++) {

                if(temp[i].compareToMeetingDate(temp[j]) == -1){

                    EntriesCustomers swap = temp[j];
                    temp[j] = temp[i];
                    temp[i] = swap;

                }
            }
        }

        ArrayList<EntriesCustomers> result = new ArrayList<EntriesCustomers>();

        for(int i = 0;i<temp.length;i++){

            result.add(temp[i]);

        }

        return result;

    }
//check for acceptable conditions of a new entry
    public ArrayList<EntriesCustomers> getResultList(){

        ArrayList<EntriesCustomers> result = new ArrayList<>();

        GregorianCalendar workTimeStart = new GregorianCalendar();
        workTimeStart.set(0,0,0,Integer.parseInt(this.workTime.substring(0,2)),Integer.parseInt(this.workTime.substring(2,4)));

        GregorianCalendar workTimeEnd = new GregorianCalendar();
        workTimeEnd.set(0,0,0,Integer.parseInt(this.workTime.substring(5,7)),Integer.parseInt(this.workTime.substring(7,9)));

        for(EntriesCustomers listEntriesCustomers : entriesCustomerses){

            //if the first entry
            if(result.size() == 0){

                if(listEntriesCustomers.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) >= workTimeStart.get(Calendar.HOUR_OF_DAY)) {

                    if (listEntriesCustomers.getMeetingStartTime().get(Calendar.MINUTE) >= workTimeStart.get(Calendar.MINUTE)) {

                        if(listEntriesCustomers.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) + listEntriesCustomers.getMeetingDuration() <= workTimeEnd.get(Calendar.HOUR_OF_DAY)) {

                            if (listEntriesCustomers.getMeetingStartTime().get(Calendar.MINUTE) <= workTimeEnd.get(Calendar.MINUTE)) {

                                result.add(listEntriesCustomers);
                            }
                        }
                    }
                }

            }
            else{

                // if all checks are satisfied isValid == true
                boolean isValid = false;

                for(EntriesCustomers inResult : result){
                    //check if this is the first record on this day
                    if(listEntriesCustomers.getMeetingStartTime().get(Calendar.YEAR) == inResult.getMeetingStartTime().get(Calendar.YEAR )
                            && listEntriesCustomers.getMeetingStartTime().get(Calendar.DAY_OF_MONTH) == inResult.getMeetingStartTime().get(Calendar.DAY_OF_MONTH)
                            && listEntriesCustomers.getMeetingStartTime().get(Calendar.MONTH) == inResult.getMeetingStartTime().get(Calendar.MONTH)){

                        if(listEntriesCustomers.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) >= workTimeStart.get(Calendar.HOUR_OF_DAY)){

                            if( listEntriesCustomers.getMeetingStartTime().get(Calendar.MINUTE) >=  workTimeStart.get(Calendar.MINUTE) ){

                                if(listEntriesCustomers.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) + listEntriesCustomers.getMeetingDuration() <= workTimeEnd.get(Calendar.HOUR_OF_DAY)){

                                    if(listEntriesCustomers.getMeetingStartTime().get(Calendar.MINUTE) <= workTimeEnd.get(Calendar.MINUTE) ) {

                                        isValid = true;

                                        //check for free record at this time
                                        if (!((listEntriesCustomers.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) + listEntriesCustomers.getMeetingDuration() <= inResult.getMeetingStartTime().get(Calendar.HOUR_OF_DAY))
                                                || (listEntriesCustomers.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) >= inResult.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) + inResult.getMeetingDuration() ))) {
                                            isValid = false;
                                        }
                                    }
                               }
                            }
                        }
                    }
                    //the first record on this day
                    else {
                        if (listEntriesCustomers.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) >= workTimeStart.get(Calendar.HOUR_OF_DAY)) {

                            if (listEntriesCustomers.getMeetingStartTime().get(Calendar.MINUTE) >= workTimeStart.get(Calendar.MINUTE)) {

                                if (listEntriesCustomers.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) + listEntriesCustomers.getMeetingDuration() <= workTimeEnd.get(Calendar.HOUR_OF_DAY)) {

                                    if (listEntriesCustomers.getMeetingStartTime().get(Calendar.MINUTE) <= workTimeEnd.get(Calendar.MINUTE)) {

                                        isValid = true;

                                    }
                                }
                            }
                        }
                    }
                }
                if(isValid){

                    result.add(listEntriesCustomers);
                }
            }


        }
        return result;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public List<EntriesCustomers> getEntriesCustomerses() {
        return entriesCustomerses;
    }

    public void setEntriesCustomerses(List<EntriesCustomers> entriesCustomerses) {
        this.entriesCustomerses = entriesCustomerses;
    }
}
