package my.springboot.domain;

import lombok.Data;
import org.springframework.stereotype.Component;
import java.util.Calendar;
import java.util.GregorianCalendar;


@Component
@Data
public class EntriesCustomers implements Comparable<EntriesCustomers> {

    private GregorianCalendar requestSubmissionTime;
    private String employeeId;
    private GregorianCalendar meetingStartTime;
    private int meetingDuration;

    //comparison of the date of entry
    @Override
    public int compareTo(EntriesCustomers o) {
        if(this.requestSubmissionTime.get(Calendar.YEAR) == o.getRequestSubmissionTime().get(Calendar.YEAR) && this.requestSubmissionTime.get(Calendar.MONTH) == o.getRequestSubmissionTime().get(Calendar.MONTH) &&
                this.requestSubmissionTime.get(Calendar.DAY_OF_MONTH) ==o.getRequestSubmissionTime().get(Calendar.DAY_OF_MONTH)){

            if(this.requestSubmissionTime.get(Calendar.HOUR_OF_DAY) == o.getRequestSubmissionTime().get(Calendar.HOUR_OF_DAY) && this.requestSubmissionTime.get(Calendar.MINUTE) == o.getRequestSubmissionTime().get(Calendar.MINUTE) &&
                    this.requestSubmissionTime.get(Calendar.SECOND) ==o.getRequestSubmissionTime().get(Calendar.SECOND)){
                return 0;
            }else{
                if(this.requestSubmissionTime.get(Calendar.HOUR_OF_DAY)>o.getRequestSubmissionTime().get(Calendar.HOUR_OF_DAY)){
                    return 1;
                }
                else{
                    if(this.requestSubmissionTime.get(Calendar.MINUTE) == o.getRequestSubmissionTime().get(Calendar.MINUTE)){
                        return 1;
                    }
                    else{
                        if(this.requestSubmissionTime.get(Calendar.SECOND) ==o.getRequestSubmissionTime().get(Calendar.SECOND)){
                            return 1;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                }
            }

        }else{
            if(this.requestSubmissionTime.get(Calendar.YEAR) >o.getRequestSubmissionTime().get(Calendar.YEAR)){
                return 1;
            }
            else{
                if(this.requestSubmissionTime.get(Calendar.MONTH) > o.getRequestSubmissionTime().get(Calendar.MONTH)){
                    return 1;
                }
                else{
                    if(this.requestSubmissionTime.get(Calendar.DAY_OF_MONTH) > o.getRequestSubmissionTime().get(Calendar.DAY_OF_MONTH)){
                        return 1;
                    }
                    else{
                        return -1;
                    }
                }
            }
        }

    }

    public int compareToMeetingDate(EntriesCustomers o){
        if(this.meetingStartTime.get(Calendar.YEAR) == o.getMeetingStartTime().get(Calendar.YEAR) && this.meetingStartTime.get(Calendar.MONTH) == o.getMeetingStartTime().get(Calendar.MONTH) &&
                this.meetingStartTime.get(Calendar.DAY_OF_MONTH) ==o.getMeetingStartTime().get(Calendar.DAY_OF_MONTH)){

            if(this.meetingStartTime.get(Calendar.HOUR_OF_DAY) == o.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) && this.meetingStartTime.get(Calendar.MINUTE) == o.getMeetingStartTime().get(Calendar.MINUTE)
                    ){
                return 0;
            }else{
                if(this.meetingStartTime.get(Calendar.HOUR_OF_DAY)>o.getMeetingStartTime().get(Calendar.HOUR_OF_DAY)){
                    return 1;
                }
                else{
                    if(this.meetingStartTime.get(Calendar.MINUTE) == o.getMeetingStartTime().get(Calendar.MINUTE)){
                        return 1;
                    }
                    else{

                            return -1;

                    }
                }
            }

        }else{
            if(this.meetingStartTime.get(Calendar.YEAR) > o.getMeetingStartTime().get(Calendar.YEAR)){
                return 1;
            }
            else{
                if(this.meetingStartTime.get(Calendar.MONTH) > o.getMeetingStartTime().get(Calendar.MONTH)){
                    return 1;
                }
                else{
                    if(this.meetingStartTime.get(Calendar.DAY_OF_MONTH) > o.getMeetingStartTime().get(Calendar.DAY_OF_MONTH)){
                        return 1;
                    }
                    else{
                        return -1;
                    }
                }
            }
        }
    }


    public GregorianCalendar getRequestSubmissionTime() {
        return requestSubmissionTime;
    }

    public void setRequestSubmissionTime(GregorianCalendar requestSubmissionTime) {
        this.requestSubmissionTime = requestSubmissionTime;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public GregorianCalendar getMeetingStartTime() {
        return meetingStartTime;
    }

    public void setMeetingStartTime(GregorianCalendar meetingStartTime) {
        this.meetingStartTime = meetingStartTime;
    }

    public int getMeetingDuration() {
        return meetingDuration;
    }

    public void setMeetingDuration(int meetingDuration) {
        this.meetingDuration = meetingDuration;
    }

}
