package my.springboot.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import my.springboot.domain.EntriesCustomers;
import my.springboot.domain.Parameters;
import org.springframework.stereotype.Service;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.*;
import java.util.Calendar;
import java.util.GregorianCalendar;


@Service
public class ResultWriter {

    public void doWrite(Parameters parameters) throws IOException {

        BufferedWriter bufferWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Output.txt"), "UTF-8"));
        BufferedWriter bufferWriterJSON = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("OutputJSON.txt"), "UTF-8"));

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String strJSON = gson.toJson(parameters);
        bufferWriterJSON.write(strJSON);

        GregorianCalendar tempGregorianCalendar = parameters.getEntriesCustomerses().get(0).getMeetingStartTime();
        String tempStringForOut = tempGregorianCalendar.get(Calendar.YEAR) + "-" + tempGregorianCalendar.get(Calendar.MONTH) + "-" + tempGregorianCalendar.get(Calendar.DAY_OF_MONTH);

        bufferWriter.write(tempStringForOut);
        bufferWriter.write("\n");
        bufferWriter.write("\n");

        for (EntriesCustomers a : parameters.getEntriesCustomerses()) {

            GregorianCalendar date = a.getMeetingStartTime();


            boolean isDate = true;

            //if a record from same day
            if (date.get(Calendar.YEAR) == tempGregorianCalendar.get(Calendar.YEAR) && date.get(Calendar.MONTH) == tempGregorianCalendar.get(Calendar.MONTH) &&
                    date.get(Calendar.DAY_OF_MONTH) == tempGregorianCalendar.get(Calendar.DAY_OF_MONTH)) {

                isDate = false;

            } else {
                tempGregorianCalendar = date;
            }

            //if a record with a new date
            if (isDate) {

                tempStringForOut= date.get(Calendar.YEAR) + "-" + date.get(Calendar.MONTH) + "-" + date.get(Calendar.DAY_OF_MONTH);
                bufferWriter.write(tempStringForOut);
                bufferWriter.write("\n");
                bufferWriter.write("\n");
            }

            tempStringForOut = a.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) + ":" + a.getMeetingStartTime().get(Calendar.MINUTE) +
                    " " + (a.getMeetingStartTime().get(Calendar.HOUR_OF_DAY) + a.getMeetingDuration()) + ":" + a.getMeetingStartTime().get(Calendar.MINUTE)
                    + " " + a.getEmployeeId();

            bufferWriter.write(tempStringForOut);
            bufferWriter.write("\n");
            bufferWriter.write("\n");

        }

        bufferWriter.close();
        bufferWriterJSON.close();
    }
}
