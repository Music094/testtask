package my.springboot.service.Interfaces;

import lombok.NonNull;
import my.springboot.domain.Parameters;

import java.util.List;

public interface ConvertFromFile {
    Parameters ConverterIntoParameters(@NonNull List<String> dataList);
}
