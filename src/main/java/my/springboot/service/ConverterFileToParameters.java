package my.springboot.service;

import lombok.NonNull;
import my.springboot.domain.EntriesCustomers;
import my.springboot.domain.Parameters;
import my.springboot.service.Interfaces.ConvertFromFile;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

@Component
public class ConverterFileToParameters implements ConvertFromFile {

    public  Parameters ConverterIntoParameters(@NonNull List<String> dataList){

        Parameters parameters = new Parameters();

        List<EntriesCustomers> listEntriesCustomers = new ArrayList<>();

        parameters.setWorkTime(dataList.get(0));

        //Parsing input parameters
        for(int i = 2;i<=dataList.size()-3;i+=4){

            EntriesCustomers entriesCustomers = new EntriesCustomers();

            String tempString = dataList.get(i);
            String tempSecondString = dataList.get(i+2);

            entriesCustomers.setRequestSubmissionTime(new GregorianCalendar(Integer.parseInt(tempString.substring(0, 4)), Integer.parseInt(tempString.substring(5, 7)),
                    Integer.parseInt(tempString.substring(8, 10)), Integer.parseInt(tempString.substring(11, 13)),
                    Integer.parseInt(tempString.substring(14, 16)), Integer.parseInt(tempString.substring(17, 19))));

            entriesCustomers.setEmployeeId(tempString.substring(20,26));

            entriesCustomers.setMeetingStartTime(new GregorianCalendar(Integer.parseInt(tempSecondString.substring(0,4)),Integer.parseInt(tempSecondString.substring(5, 7)),
                    Integer.parseInt(tempSecondString.substring(8, 10)), Integer.parseInt(tempSecondString.substring(11, 13)),
                    Integer.parseInt(tempSecondString.substring(14, 16))));

            entriesCustomers.setMeetingDuration(Integer.parseInt(tempSecondString.substring(17,18)));

            listEntriesCustomers.add(entriesCustomers);

        }

        parameters.setEntriesCustomerses(listEntriesCustomers);

        return parameters;
    }

}
