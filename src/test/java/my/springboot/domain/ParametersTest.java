package my.springboot.domain;

import junit.framework.TestCase;
import my.springboot.service.ConverterFileToParameters;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Music on 19.03.2017.
 */
public class ParametersTest extends TestCase {
    Parameters test;
    ConverterFileToParameters cnvTest;
    List<String> dataListTest;

    public void setUp() throws Exception {
        test = new Parameters();
        cnvTest = new ConverterFileToParameters();
        dataListTest = new ArrayList<>();
        dataListTest.add("0900 1730");
        dataListTest.add(" ");
        dataListTest.add("2011-03-17 10:17:06 EMP001");
        dataListTest.add(" ");
        dataListTest.add("2011-03-21 09:00 2");
        dataListTest.add(" ");
        dataListTest.add("2011-03-16 12:34:56 EMP002");
        dataListTest.add(" ");
        dataListTest.add("2011-03-21 09:00 2");
        dataListTest.add(" ");
        dataListTest.add("2011-03-16 09:28:23 EMP003");
        dataListTest.add(" ");
        dataListTest.add("2011-03-22 14:00 2");
        dataListTest.add(" ");
        dataListTest.add("2011-03-17 11:23:45 EMP004");
        dataListTest.add(" ");
        dataListTest.add("2011-03-22 16:00 1");
        dataListTest.add(" ");
        dataListTest.add("2011-03-15 17:29:12 EMP005");
        dataListTest.add(" ");
        dataListTest.add("2011-03-21 16:00 3");

        test = cnvTest.ConverterIntoParameters(dataListTest);
    }

    public void tearDown() throws Exception {

    }

    public void testSortEntriesCustomersByRequestTime() throws Exception {
        test.showMe();
        System.out.println("Sort by request");
        test.setEntriesCustomerses(test.sortEntriesCustomersByRequestTime());
        test.showMe();
        System.out.println("");

    }

    public void testSortEntriesCustomersByMettingTime() throws Exception {
        System.out.println("Sort by meeting time");
        test.setEntriesCustomerses(test.sortEntriesCustomersByMettingTime());
        test.showMe();
        System.out.println("");
    }

    public void testGetResultList() throws Exception {
        System.out.println("Result");
        test.setEntriesCustomerses(test.sortEntriesCustomersByRequestTime());
        test.setEntriesCustomerses(test.getResultList());
        test.setEntriesCustomerses(test.sortEntriesCustomersByMettingTime());
        test.showMe();
    }
}