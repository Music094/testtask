package my.springboot.service;

import junit.framework.TestCase;
import my.springboot.domain.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Music on 19.03.2017.
 */

public class ConverterFileToParametersTest extends TestCase {

    ConverterFileToParameters test;
    List<String> dataList;
    @Before
    public void setUp() throws Exception {
        test = new ConverterFileToParameters();
        dataList = new ArrayList<>();
        dataList.add("0900 1730");
        dataList.add(" ");
        dataList.add("2011-03-17 10:17:06 EMP001");
        dataList.add(" ");
        dataList.add("2011-03-21 09:00 2");
        dataList.add(" ");
        dataList.add("2011-03-16 12:34:56 EMP002");
        dataList.add(" ");
        dataList.add("2011-03-21 09:00 2");
        dataList.add(" ");
        dataList.add("2011-03-16 09:28:23 EMP003");
        dataList.add(" ");
        dataList.add("2011-03-22 14:00 2");
        dataList.add(" ");
        dataList.add("2011-03-17 11:23:45 EMP004");
        dataList.add(" ");
        dataList.add("2011-03-22 16:00 1");
        dataList.add(" ");
        dataList.add("2011-03-15 17:29:12 EMP005");
        dataList.add(" ");
        dataList.add("2011-03-21 16:00 3");
        dataList.add(" ");
        dataList.add("2010-03-15 10:20:12 EMP006");
        dataList.add(" ");
        dataList.add("2011-03-21 16:00 1");
        dataList.add(" ");
        dataList.add("2011-03-15 17:29:12 EMP007");
        dataList.add(" ");
        dataList.add("2012-05-12 16:00 7");
    }

    public void tearDown() throws Exception {

    }
    @Test
    public void testConverterIntoParameters() throws Exception {
        Parameters parameters = test.ConverterIntoParameters(dataList);
        parameters.showMe();
    }
}